#include <sdsl/suffix_trees.hpp>
#include <string>

using namespace sdsl;
using namespace std;

int main()
{ 
  csa_wt<wt_huff<rrr_vector<127> >, 512, 1024> index;
  string index_filename = "tmp_file";
  construct(index, "problem_input", 1);
  std::cout << "Index construced" << std::endl;
  store_to_file(index, index_filename);
  std::cout << "Done." << std::endl;
}


