cd ext
mkdir local
git clone https://github.com/simongog/sdsl-lite
cd sdsl-lite
./install.sh ../local
cd ..;
cd ..;
cd src
make
./triggerer.x
valgrind --vgdb-error=1 --leak-check=full --show-reachable=yes ./triggerer.x
